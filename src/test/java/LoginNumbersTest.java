// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
public class LoginNumbersTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    System.setProperty("webdriver.gecko.driver","E:\\GekoDriver\\geckodriver-v0.15.0-win64\\geckodriver.exe");
    driver = new FirefoxDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void logintylkoliczby() {
    driver.get("https://www.google.com/");
    driver.manage().window().setSize(new Dimension(1891, 1018));
    driver.findElement(By.name("q")).click();
    driver.findElement(By.name("q")).sendKeys("poczta o2 logowanie");
    driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
    assertThat(driver.findElement(By.cssSelector(".g > div > div > .rc .LC20lb")).getText(), is("Poczta o2 - najszybciej rozwijająca się poczta"));
    assertThat(driver.findElement(By.cssSelector(".g:nth-child(1) > div > .rc .LC20lb")).getText(), is("Poczta o2 logowanie - Zaloguj się | Logowanie"));
    driver.findElement(By.cssSelector(".g > div > div > .rc .LC20lb")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("TESTOWANIE");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("testowanie");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("TestOwAnIe#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("t");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Testowanie#404Testowanie#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Testowanie#404Testowanie#404Testowanie#404Testowanie#404Testowanie#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("тестирование");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys(" Testowanie#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Testow anie#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Testowanie#404 ");
    driver.findElement(By.id("login-button")).click();
    driver.get("https://poczta.o2.pl/zaloguj");
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Tęstowanię#404");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("efbyj");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("<body>");
    driver.findElement(By.id("login-button")).click();
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("login")).sendKeys("404");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).sendKeys("Testowanie#404");
    driver.findElement(By.id("login-button")).click();
  }
}
